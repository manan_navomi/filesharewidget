//number to string to recognize as session file
var fs = require('fs');
var express = require('express');
var path = require('path');

  module.exports = (app) => {
      var sampleFile;

      app.post('/index', function(req, res) {
          if (!req.files) {
            res.send('No files were uploaded.');
            return;
          }

          // console.log('req: ',req.body.sessionId);

          sampleFile = req.files;
          sampleFile.file.name = req.body.sessionId+"_"+sampleFile.file.name;

          sampleFile.file.mv(__dirname + '/upload/'+sampleFile.file.name, function(err) {
            // console.log("File has uploaded to upload folder....");
            if (err) {
              console.log("Error uploading: ",err);
              res.status(500).send(err);
            } else {
              res.send('File uploaded!');
            }
          });
      });
  }
