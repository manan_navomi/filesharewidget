var express = require('express');
var route = require('./route');
var fileUpload = require('express-fileupload');
var path = require('path');
var fs = require('fs');
var https = require('https');

var privateKey  = fs.readFileSync('ssl/privkey.pem', 'utf8');
var certificate = fs.readFileSync('ssl/fullchain.pem', 'utf8');
var credentials = {key: privateKey, cert: certificate};

var app = express();
var httpsServer = https.createServer(credentials, app);

app.use(fileUpload());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/upload', express.static(__dirname + '/upload'));
app.get('/fileList',function(req,res){
  var fileArray = [];

    fs.readdir('upload', (err, files) => {
      files.forEach(file => {
        var stats = fs.statSync('upload/'+file);
        var fileSizeInKb = stats.size / 1000.0;

        var tempFile = {filename: file,filesize: fileSizeInKb};

        //console.log(file);
        //console.log(fileSizeInKb);

        fileArray.push(tempFile);
        // res.json({filename:file});
        tempFile = {}
      });
      res.json(fileArray);
    });

    // res.status(200);
    // res.send(fileArray);
});
route(app)

httpsServer.listen(443);
console.log("Port Linsting 443");
