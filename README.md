# README #

FileSharing Widget uses Liveperson Engagement Window SDK
for Visitor side and Agent Workspace SDK for Agent Side.

## Removing files ##

The FileSharing Widget must remove files 30 minutes after they are
uploaded. This is currently achieved via the following BASH command:

/usr/bin/find /var/opt/filesharewidget/upload/*.* -type f -mmin +30 -exec rm {} \;

To check for old files and remove them every minute, add the following
line to crontab of appropriate user (E.g. root):

* * * * * /usr/bin/find /var/opt/filesharewidget/upload/*.* -type f -mmin +30 -exec rm {} \;

That's i!