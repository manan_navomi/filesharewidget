var fileshare = angular.module('fileshare');

fileshare.controller('FileShareController',function($scope,$http,$window){
    var url = 'https://'+window.location.hostname+'/fileList';     // URL

    $scope.files = [];
    $scope.error = '';

    lpTag.agentSDK.init();
    var onSuccess = function(data){
        // console.log('Callback Success !',data);
        $scope.rtSessionId = data;
    }

    var onError = function(error){
        console.log('Callback Error: ',error);
    }

    lpTag.agentSDK.get('chatInfo.rtSessionId',onSuccess,onError);


    var getFiles = function(){
        $scope.files = [];
        $http.get(url).
        success(function(response){
          response.forEach(file => {
            //Filter to display files which contain chatsessionId
            if($scope.rtSessionId != null || $scope.rtSessionId != ""){
                if(file.filename.includes($scope.rtSessionId)){
                    $scope.files.push(file);
                }
            }//ELSE CASE: no file list appear to the agent
          });
          // console.log('files:'+ $scope.files);
        }).
        error(function(error){
          $scope.error = error;
        });
    }

    $scope.refreshfiles = getFiles;
    getFiles();

});
