    var widgeSDK,
    widgetSDKAPI = lpTag.LPWidgetSDK.API;

    window.addEventListener("load",function chatsessionid(){
        var bindEvent = [widgetSDKAPI.events.CONVERSATION_INFO],
        opts = {bind:{}};

        bindEvent.forEach(function (eventName){
            opts.bind[eventName] = {func:_onEvent,context:this};
        });
        LPWidgetSDK = lpTag.LPWidgetSDK.init(opts);
    }.bind(this));

    var rtSessionId=null;
    function _onEvent(eventData) {
        //console.log(eventData);
        if(eventData && widgetSDKAPI.events.CONVERSATION_INFO === eventData.type){
            rtSessionId = eventData.data.conversationId;
            // console.log("rtSessionId: "+rtSessionId);
            return rtSessionId;
        }
    }
